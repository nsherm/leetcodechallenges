/**
 * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 *
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 *
 * @param nums
 * @param target
 * @return {*[]}
 */
let twoSum = function(nums, target) {
    // Create an object to hold indexes of the values we have seen
    // [Value = index]
    let seenValues = {};

    // Loop through all of the possible numbers
    for (let index = 0; index < nums.length; index++) {
        // Have we found the answer yet?  i.e. Do we have the complement
        if (typeof (seenValues[target - nums[index]]) === 'number') {
            return [seenValues[target - nums[index]], index];
        }

        // Store the index of this number with a key of the numbers value
        seenValues[nums[index]] = index;
    }
};

console.log(twoSum([2, 7, 11, 15], 9));
