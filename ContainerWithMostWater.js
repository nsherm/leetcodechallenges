/**
 * Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai). n vertical
 * lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together
 * with x-axis forms a container, such that the container contains the most water.
 *
 * Note: You may not slant the container and n is at least 2.
 *
 * @param {number[]} height Array of heights
 *
 * @return {number} Maximum area
 */
let maxArea = function(height) {
    let leftPointer = 0;
    let rightPointer = height.length-1;
    let maxArea = 0;
    
    // Loop until the pointers meet
    while (leftPointer < rightPointer) {
        let currentArea = 0;

        // The largest area possible is determined by the smallest height * width
        // Once calculated, we move the smaller of the two pointers incase there is a larger height that even
        // with the reduction in width results in a larger area.

        if (height[leftPointer] > height[rightPointer]) {
            currentArea = height[rightPointer] * calcCurrentWidth(leftPointer, rightPointer);
            // Move the right pointer left as the the current area is restricted
            // by the smaller right pointer
            rightPointer--;
        } else {
            currentArea = height[leftPointer] * calcCurrentWidth(leftPointer, rightPointer);

            // Move the left pointer right as the current area is restricted by
            // the smaller left pointer
            leftPointer++;
        }

        // Is this the largest area we have found so far?
        if (currentArea > maxArea) {
            maxArea = currentArea;
        }
    }
    return maxArea;
};

let calcCurrentWidth = function(leftPointer, rightPointer) {
    return rightPointer - leftPointer;
};

console.log(maxArea([1,8,6,2,5,4,8,3,7]));
